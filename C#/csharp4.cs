public interface IProduct
{
    void SetPrice(int price);
}

public class Iphone : IProduct
{
    public void SetPrice(int price)
    {
        //sets price
    }
}

public class Ipad : IProduct
{
    public void SetPrice(int price)
    {
        //sets price
    }
}
public class IphoneFactory
{
    public Iphone CreateNewIphone(int price)
    {
        Iphone newIphone = new Iphone();
        newIphone.SetPrice(price);
        return newIphone;
    }
}

public class IpadFactory
{
    public Ipad CreateNewIpad(int price)
    {
        Ipad newIpad = new Ipad();
        newIpad.SetPrice(price);
        return newIpad;
    }
}
