abstract class Product
{
	public abstract int GetPrice();
}

class Iphone : Product
{
	public override int GetPrice()
	{
		return 500;
	}
}

class Ipad : Product
{
	public override int GetPrice()
	{
		return 400;
	}
}

class Bicycle : Product
{
	public override int GetPrice()
	{
		return 400;
	}
}

class ProductManager
{
	// Sum price for firstStartup service for all phones and tablets in the order
	public int GetPriceForFirstStartupServices()
	{
		
	}
}

