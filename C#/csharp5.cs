public class UserManager
{
	public void RegisterUser(string name, int age, string email, string street, int number, string city)
	{
		var address = new Address();
		address.City = city;
		address.Street = street;
		address.Number = number;

		var user = new User();
		user.Address = address;

		var mailManager = new MailManager();
		mailManager.SendConfirmationEMail();
	}
}

public class User
{
	public string Name { get; set; }
	public int Age { get; set; }
	public Address Address { get; set; }
}

public class Address
{
	public string City { get; set; }
	public string Street { get; set; }
	public int Number { get; set; }
}

public class MailManager
{
	public void SendConfirmationEMail()
	{
		// sends e-mail
	}
}
