class User
{
	public int Age {get; set;}
}

public class Demo
{
	public void RunDemo()
	{
		int value = 5;
		int copy = value;
		value = 10;
		Console.WriteLine(copy);

		User user = new User();
		user.Age = 20;
		User userCopy = user;
		user.Age = 30;
		Console.WriteLine(userCopy.Age);

		ChangeUserAge(user.Age);
		Console.WriteLine(user.Age);
	}

	public void ChangeUserAge(int age)
	{
		age = 40;
	}
}
