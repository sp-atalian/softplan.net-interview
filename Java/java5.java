public class UserManager
{
    public void registerUser(String name, int age, String email, String street, int number, String city)
    {
        var address = new Address();
        address.city = city;
        address.street = street;
        address.number = number;

        var user = new User();
        user.address = address;

        var mailManager = new MailManager();
        mailManager.sendConfirmationEMail();
    }
}

class User
{
    String name;
    int age;
    Address address;
}

class Address
{
    String city;
    String street;
    int number;
}

class MailManager
{
    void sendConfirmationEMail()
    {
        // sends e-mail
    }
}