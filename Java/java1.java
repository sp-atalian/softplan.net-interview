class User {
    private int age;

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return this.age;
    }
}

public class Demo {
    public void runDemo() {
        int value = 5;
        int copy = value;
        value = 10;
        System.out.println(copy);

        User user = new User();
        user.setAge(20);
        User userCopy = user;
        user.setAge(30);
        System.out.println(userCopy.getAge());
    }
}
