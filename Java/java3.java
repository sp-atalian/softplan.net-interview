abstract class Product {
    public abstract int getPrice();
}

class Iphone extends Product {
    @Override
    public int getPrice() {
        return 500;
    }
}

class Ipad extends Product {
    @Override
    public int getPrice() {
        return 400;
    }
}

class Bicycle extends Product {
    @Override
    public int getPrice() {
        return 400;
    }
}

public class ProductManager {
    // Sum price for firstStartup service for all phones and tablets in the order
    public int getPriceForFirstStartupServices() {


    }
}